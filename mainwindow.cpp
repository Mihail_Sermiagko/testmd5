#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    QString number = findNumber(ui->lineEditMd5->text());
    for (int i = number.size(); i < 8 ; i++) {
        number = "0" + number;
    }

    ui->lineEditResult->setText(number);
}

std::string MainWindow::stdStringToMd5Hash(std::string string)
{
    MD5 md5(string);
    return md5.hexdigest();
}

QString MainWindow::findNumber(QString md5String)
{
    int time1 = QDateTime::currentSecsSinceEpoch();
    std::string sdtMd5String = md5String.toStdString();
    int numberMd5 = -1;
    QVector <int> vector;
    int max =100000001;
    int min = 10000000;
    int step = 100000;
    int localMax = step + min;

    QFutureWatcher<void> futureWatcher;
    connect ( this , &MainWindow::signalStopFuter, &futureWatcher, &QFutureWatcher<void>::cancel);

    for (int i = min; i < max; i++) {
        if(i < localMax){
            vector.append(i);
        }
        else {
            futureWatcher.setFuture(QtConcurrent::map(vector,[&](const int& d){
                if(sdtMd5String == stdStringToMd5Hash(std::to_string(d))){
                    qDebug()<<d<<QString::fromStdString(stdStringToMd5Hash(std::to_string(d)));
                    numberMd5 = d;
                    signalStopFuter();
                }
            }));
            futureWatcher.waitForFinished();

            vector.clear();
            vector.append(i);
            localMax = i + step;
            if(numberMd5 != -1){
                break;
            }
        }
    }

    if(numberMd5 == -1){
        max = 10000001;
        min = 0;
        step = 100000;
        localMax = step + min;
        for (int i = min; i < max; i++) {
            if(i < localMax){
                vector.append(i);
            }
            else {
                futureWatcher.setFuture(QtConcurrent::map(vector,[&](const int& d){
                    std::string number;
                    if(d < 10){
                        number = "0000000" + std::to_string(d);
                    }
                    else if(d < 100){
                        number = "000000" + std::to_string(d);
                    }
                    else if(d < 1000){
                        number = "00000" + std::to_string(d);
                    }
                    else if(d < 10000){
                        number = "0000" + std::to_string(d);
                    }
                    else if(d < 100000){
                        number = "000" + std::to_string(d);
                    }
                    else if(d < 1000000){
                        number = "00" + std::to_string(d);
                    }
                    else if(d < 10000000){
                        number = "0" + std::to_string(d);
                    }
                    if(sdtMd5String == stdStringToMd5Hash(number)){
                        qDebug()<<d<<QString::fromStdString(stdStringToMd5Hash(number));
                        numberMd5 = d;
                        signalStopFuter();
                    }
                }));
                futureWatcher.waitForFinished();

                vector.clear();
                vector.append(i);
                localMax = i + step;
                if(numberMd5 != -1){
                    break;
                }
            }
        }
    }

    qDebug()<<"time:"<<QDateTime::currentSecsSinceEpoch() - time1;
    return QString::number(numberMd5);

}


