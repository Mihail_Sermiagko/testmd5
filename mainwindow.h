#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "QDebug"
#include "md5.h"
#include <QtConcurrent>
#include <QMutex>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void on_pushButton_clicked();
    std::string stdStringToMd5Hash(std::string string);
    QString findNumber(QString md5String);

signals:
    void signalStopFuter();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
