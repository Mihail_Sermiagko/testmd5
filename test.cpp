#include "test.h"

Test::Test()
{

}

void Test::stdStringToMd5Hash()
{
    MainWindow mw;
    std::string string;
    QCOMPARE(mw.stdStringToMd5Hash("12345678"),"25d55ad283aa400af464c76d713c07ad");
    QCOMPARE(mw.stdStringToMd5Hash("12342678"),"d2d0b6d8563b92c875215ca9939c1848");
    QCOMPARE(mw.stdStringToMd5Hash("12342672"),"30cc9f4e121934db790f36494ec438dd");
}

void Test::findNumber()
{
    MainWindow mw;
    QCOMPARE(mw.findNumber("25d55ad283aa400af464c76d713c07ad"),"12345678");
}


