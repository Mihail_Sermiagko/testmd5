#ifndef TEST_H
#define TEST_H

#include <QObject>
#include <QtTest>
#include "mainwindow.h"

class Test : public QObject
{
    Q_OBJECT
public:
    Test();

private slots:
    void stdStringToMd5Hash();
    void findNumber();
};

#endif // TEST_H
